# Lingering Intent Service #

An Intent Services (android.app.IntentService) immediately stops itself when all tasks are done. This repo contains a lingering variant which stays around for a little while longer. This allows for a little state to be kept for the linger duration. A login session could be a nice candidate. The Persistent subclass of Linger service employs a Preference file to maintain state across lingering sessions.

### What is this repository for? ###

This repository contains the LingerService and PersistentLingerService source and an example use.

### How do I get set up? ###

The LingerService and PersistentLingerService are IntentService like classes which must be subclasses in an application to be used.


### Contribution guidelines ###

These sources are experimental in nature and available under Apache License Version 2.0.



