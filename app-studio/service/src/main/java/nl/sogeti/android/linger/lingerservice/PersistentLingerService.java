/**
 Copyright 2014 René de Groot

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
package nl.sogeti.android.linger.lingerservice;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;

import java.util.Map;

/**
 * Based on android.app.IntentService
 * <p/>
 * Created by grootren on 15-11-14.
 */
public abstract class PersistentLingerService extends LingerService {

    private final String mName;
    private SharedPreferences state;

    /**
     * Call this constructor from your default
     *
     * @param name           name used to create a named thread
     * @param lingerDuration number of seconds the service should linger after work has finished
     */
    public PersistentLingerService(String name, int lingerDuration) {
        super(name, lingerDuration);
        this.mName = name;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        state = getSharedPreferences("servicestate:" + mName, Activity.MODE_PRIVATE);
    }

    public void putInt(String key, int value) {
        state.edit().putInt(key, value).apply();
    }

    public void putString(String key, String value) {
        state.edit().putString(key, value).apply();
    }

    private void loadInstanceState() {
        final Bundle savedInstanceState = new Bundle();
        final Map<String, ?> all = state.getAll();
        for (String key : all.keySet()) {
            Object value = all.get(key);
            if (value instanceof Integer) {
                savedInstanceState.putInt(key, (Integer) value);
            } else if (value instanceof String) {
                savedInstanceState.putString(key, (String) value);
            }
        }
        onRestoreInstanceState(savedInstanceState);
    }

    @Override
    protected void didCreate() {
        super.didCreate();
        loadInstanceState();
    }

    /**
     * Called when the session is restored
     *
     * @param savedInstanceState
     */
    protected abstract void onRestoreInstanceState(Bundle savedInstanceState);

}
