/**
 Copyright 2014 René de Groot

 Licensed under the Apache License, Version 2.0 (the "License");
 you may not use this file except in compliance with the License.
 You may obtain a copy of the License at

 http://www.apache.org/licenses/LICENSE-2.0

 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS,
 WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 See the License for the specific language governing permissions and
 limitations under the License.
 */
package nl.sogeti.android.linger.lingerservice;

import android.app.Service;
import android.content.Intent;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;

/**
 * Based on android.app.IntentService
 * <p/>
 * Created by grootren on 15-11-14.
 */
public abstract class LingerService extends Service {

    private final long mDuration;
    private volatile Looper mServiceLooper;
    private volatile ServiceHandler mServiceHandler;
    private final String mName;
    private boolean isFirstRun;

    private final class ServiceHandler extends Handler {
        public ServiceHandler(Looper looper) {
            super(looper);
        }

        @Override
        public void handleMessage(Message msg) {
            if (msg.obj instanceof Intent) {
                onHandleIntent((Intent) msg.obj);
                final int startId = msg.arg1;
                this.postDelayed(new RunStopSelf(startId), mDuration);
            }
        }
    }

    private final class RunStopSelf implements Runnable {
        private final int startId;

        public RunStopSelf(int startId) {
            this.startId = startId;
        }

        @Override
        public void run() {
            stopSelf(startId);
        }
    }

    /**
     * Call this constructor from your default
     *
     * @param name           name used to create a named thread
     * @param lingerDuration number of seconds the service should linger after work has finished
     */
    public LingerService(String name, int lingerDuration) {
        super();
        mName = name;
        mDuration = 1000L * lingerDuration;
        isFirstRun = true;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        HandlerThread thread = new HandlerThread("LingerService[" + mName + "]");
        thread.start();

        mServiceLooper = thread.getLooper();
        mServiceHandler = new ServiceHandler(mServiceLooper);
    }

    @Override
    final public int onStartCommand(Intent intent, int flags, int startId) {
        if (isFirstRun) {
            isFirstRun = false;
            if (intent == null) {
                stopSelf();
            } else {
                didCreate();
            }
        }

        Message msg = mServiceHandler.obtainMessage();
        msg.arg1 = startId;
        msg.obj = intent;
        mServiceHandler.sendMessage(msg);

        // Start sticky so ungraceful stop can be detected and relayed into a didDestroy()
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        mServiceLooper.quit();
        didDestroy();
    }

    @Override
    final public IBinder onBind(Intent intent) {
        return null;
    }

    /**
     * This method is invoked on the worker thread only one Intent is processed at a time.
     *
     * @see android.app.IntentService#onHandleIntent(android.content.Intent)
     */
    protected abstract void onHandleIntent(Intent intent);

    /**
     * This method is invoked on the main thread when the first is first created with an intent
     */
    protected void didCreate() {
    }

    /**
     * This method is invoked on the main thread when the service is destroyed
     */
    protected void didDestroy() {
    }
}
