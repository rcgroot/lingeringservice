package nl.sogeti.android.linger;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.webkit.WebView;

/**
 * Show about info
 * 
 * @author rene (c) 2014, Sogeti B.V.
 */
public class AboutDialogFragment extends DialogFragment
{
   public AboutDialogFragment()
   {
   }

   @Override
   public Dialog onCreateDialog(Bundle savedInstanceState)
   {
      WebView webview = new WebView(getActivity());
      webview.loadUrl("file:///android_asset/about.html");
      return new AlertDialog.Builder(getActivity()).setTitle(R.string.title_about)
            .setPositiveButton(android.R.string.ok, null).setView(webview).create();
   }
}
