package nl.sogeti.android.linger;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import butterknife.ButterKnife;
import butterknife.InjectView;
import butterknife.OnClick;
import de.greenrobot.event.EventBus;


public class MainActivity extends Activity {

    @InjectView(R.id.activity_main_status)
    TextView status;
    @InjectView(R.id.activity_main_duration)
    TextView taskDuration;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.inject(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onPause() {
        EventBus.getDefault().unregister(this);
        super.onPause();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_about) {
            FragmentTransaction ft = getFragmentManager().beginTransaction();
            Fragment previous = getFragmentManager().findFragmentByTag("dialog");
            if (previous != null) {
                ft.remove(previous);
            }
            ft.addToBackStack(null);
            AboutDialogFragment about = new AboutDialogFragment();
            about.show(ft, "dialog");


            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @OnClick(R.id.activity_main_login)
    public void login() {
        Intent intent = new Intent(this, DemoLingerService.class);
        intent.setAction(DemoLingerService.ACTION_LOGIN);
        startService(intent);
    }

    @OnClick(R.id.activity_main_execute)
    public void executeTask() {
        Intent intent = new Intent(this, DemoLingerService.class);
        intent.setAction(DemoLingerService.ACTION_TASK);
        final Integer duration = Integer.valueOf(taskDuration.getText().toString());
        intent.putExtra(DemoLingerService.EXTRA_DURATION, duration);
        startService(intent);
    }

    @OnClick(R.id.activity_main_logout)
    public void logout()   {
        Intent intent = new Intent(this, DemoLingerService.class);
        intent.setAction(DemoLingerService.ACTION_LOGOUT);
        startService(intent);
    }

    public void onEventMainThread(String event) {
        status.setText(event);
    }
}
