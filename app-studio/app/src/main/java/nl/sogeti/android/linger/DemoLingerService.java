package nl.sogeti.android.linger;

import android.content.Intent;
import android.os.Bundle;

import de.greenrobot.event.EventBus;
import nl.sogeti.android.linger.lingerservice.PersistentLingerService;

/**
 * Gives an example on how extending the Lingering Service works.
 * <p/>
 * Created by grootren on 15-11-14.
 */
public class DemoLingerService extends PersistentLingerService {
    public static final String ACTION_TASK = "ACTION_TASK";
    public static final String ACTION_LOGOUT = "ACTION_LOGOUT";
    public static final String ACTION_LOGIN = "ACTION_LOGIN";
    public static final String EXTRA_DURATION = "EXTRA_DURATION";

    private static final String KEY_PING = "KEY_PING";
    public static final int LINGER_MINUTES = 5;
    private int operationCount;
    private boolean isLoggedIn;

    public DemoLingerService() {
        super("DemoLingerService", LINGER_MINUTES * 60);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        final String action = intent.getAction();
        final EventBus eventBus = EventBus.getDefault();
        switch (action) {
            case ACTION_LOGIN:
                if (!isLoggedIn) {
                    isLoggedIn = true;
                    eventBus.post("Did successfully log in " );
                } else {
                    eventBus.post("Failed to login, already logged in " );
                }
                break;
            case ACTION_LOGOUT:
                stopSelf();
                break;
            case ACTION_TASK:
                if (isLoggedIn) {
                    int time = intent.getIntExtra(EXTRA_DURATION, 0);
                    try {
                        eventBus.post("Incremented ping to " + (operationCount + 1));
                        Thread.sleep(time);
                        incrementPing();
                        eventBus.post("Finished task on ping " + operationCount);
                    } catch (InterruptedException e) {
                        eventBus.post("InterruptedException ");
                    }
                } else {
                    eventBus.post("Failed operation during login ");
                    stopSelf();
                }
                break;
            default:
                eventBus.post("Received unknown action " + action);
                stopSelf();
                break;

        }
    }

    private void incrementPing() {
        operationCount++;
        putInt(KEY_PING, operationCount);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        if (savedInstanceState == null) {
            operationCount = 0;
            EventBus.getDefault().post("Fresh ping  "+operationCount);
        } else {
            operationCount = savedInstanceState.getInt(KEY_PING, 0);
            EventBus.getDefault().post("Restored ping  " + operationCount);
        }
    }

    @Override
    protected void didDestroy() {
        if (isLoggedIn) {
            EventBus.getDefault().post("Did logout at ping " + operationCount);
        }
    }
}

